FROM tomcat:7-jre8

RUN wget https://github.com/OpenRock/OpenAM/releases/download/13.0.0/OpenAM-13.0.0.zip && \
    unzip -d unpacked *.zip && \
    mv unpacked/openam/OpenAM*.war $CATALINA_HOME/webapps/openam.war && \
    rm -rf *.zip unpacked

ENV CATALINA_OPTS="-Xmx2048m -server"

EXPOSE 8080

CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]

